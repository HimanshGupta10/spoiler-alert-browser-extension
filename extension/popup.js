$(document).ready(function () {
  localArray = [];
  
  if(localArray.length == 0){
    chrome.storage.sync.get( {key : []}, function(items){
      var removeitem = undefined
      items = jQuery.grep(items, function(value){
        return value != removeitem
      })
      localArray = items
    })
  }

  if (!localStorage.keyWords) {
    localStorage.setItem('keyWords', JSON.stringify(localArray));
    var obj = {key: localArray}
    chrome.storage.sync.set(obj, function(){
      console.log("synced successfully!")
    })
  }
  
  loadKeyWords();
  
  function loadKeyWords() {
      $('#keyWords').html('');
      localArray = JSON.parse(localStorage.getItem('keyWords'));
      for(var i = 0; i < localArray.length; i++) {
        $('#keyWords').prepend('<li><input class="check" name="check" type="checkbox">'+localArray[i]+'</li>'); 
          }
      }
  
  $('#add').click( function() {
     var Description = $('#description').val();
    if($("#description").val() === '') {
      $('#alert').html("<strong>Warning!</strong> You left the to-do empty");
      $('#alert').fadeIn().delay(1000).fadeOut();
      return false;
     }
     $('#form')[0].reset();
     localArray.push(Description);
     localStorage.setItem('keyWords', JSON.stringify(localArray));
     var obj = {key: localArray}
     chrome.storage.sync.set(obj, function(){
        console.log("synced successfully!")
      })
     loadKeyWords();
     return false;
  });
  
  $('#clear').click( function() {
  window.localStorage.clear();
  var items = []
  var obj = {key : items}
  chrome.storage.sync.set(obj, function() {
    console.log("synced Successfully!")
  })
  location.reload();
  return false;
  });
  
  $('#clearChecked').click(function() {
    currentArray = [];
    $('.check').each(function() {
      var $curr = $(this);
      if (!$curr.is(':checked')) {
        var value = $curr.parent().text();
        currentArray.push(value);
        localStorage.setItem('keyWords', JSON.stringify(currentArray));
        var obj = {key: currentArray}
        chrome.storage.sync.set(obj, function(){
          console.log("synced successfully!")
        })
        loadKeyWords();
      } else {
        var value = $curr.parent().text()
        $curr.parent().remove();
        chrome.storage.sync.get({key : []}, function(items, value){
          var removeitem = value
          items = jQuery.grep(items, function(value){
            return value != removeitem;
          })  
          var obj = {key : items}
          chrome.storage.sync.set(obj, function(){
            console.log("synced Successfully!")
          })
        })
      }
    });
  });
  
}); 